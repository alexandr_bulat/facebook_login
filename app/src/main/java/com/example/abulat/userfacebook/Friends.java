package com.example.abulat.userfacebook;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.facebook.share.ShareApi;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.RuntimeExceptionDao;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import adapters.MoviesAdapter;
import customlists.Friedlist;
import customlists.OrmLite;
import database.DatabaseHelper;

/**
 * Created by abulat on 7/18/16.
 */
@SuppressWarnings("deprecation")
public class Friends extends AppCompatActivity {

    private List<Friedlist> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private MoviesAdapter mAdapter;
    DialogView dialogView;
    private String url;
    RuntimeExceptionDao<OrmLite, Integer> cont;
    int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    ShareDialog shareDialog;
    boolean set = true;
    EditText editText;
    Bitmap thumbnail;
    DatabaseHelper helper;
    private SharedPreferences mPrefs;
    private SharedPreferences mPrefs1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friends_list);


        prepareMovieData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        getMenuInflater().inflate(R.menu.menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id) {
            case R.id.create_new:
                show();
                break;
            case R.id.create_new1:
                DIalog();
                break;
            case R.id.ormLite:
                UsersofORM();
                break;
        }

        return super.onOptionsItemSelected(item);

    }

    private void prepareMovieData() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        editText = (EditText) findViewById(R.id.editText);
        shareDialog = new ShareDialog(this);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }
        Intent intent = getIntent();
        String jsondata = intent.getStringExtra("jsondata");
        helper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        cont = helper.getContcRuntimeExcepctionDao();
        JSONArray friendslist;

        try {
            mPrefs = getSharedPreferences("my",MODE_PRIVATE);
                friendslist = new JSONArray(mPrefs.getString("jsondata",""));

            for (int l = 0; l < friendslist.length(); l++) {
                Friedlist movie = new Friedlist();
                movie.setTitle(friendslist.getJSONObject(l).getString("name"));
                JSONObject picture = friendslist.getJSONObject(l).getJSONObject("picture");
                JSONObject picdata = picture.getJSONObject("data");
                url = picdata.getString("url");
                movie.setImage(url);
                cont.create(new OrmLite(friendslist.getJSONObject(l).getString("name"), url));
                movieList.add(movie);
                mAdapter = new MoviesAdapter(this, movieList);
                dialogView = new DialogView(this);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
    }

    public void UsersofORM() {

        List<OrmLite> notes = cont.queryForAll();
        mAdapter = new MoviesAdapter(this, notes);
        recyclerView.setAdapter(mAdapter);
        OpenHelperManager.releaseHelper();
    }

    public void show() {

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.custom);
        TextView text = (TextView) dialog.findViewById(R.id.dismiss);
        TextView textload = (TextView) dialog.findViewById(R.id.loadFoto);
        TextView textGallary = (TextView) dialog.findViewById(R.id.loadFromGallary);
        editText = (EditText) dialog.findViewById(R.id.editText);
        textGallary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(
                        Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                startActivityForResult(
                        Intent.createChooser(intent, "Select File"),
                        SELECT_FILE);
            }
        });
        textload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, REQUEST_CAMERA);

            }
        });
        text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == SELECT_FILE)

                onSelectFromGalleryResult(data);

            else if (requestCode == REQUEST_CAMERA) {
                onCaptureImageResult(data);
            }
        }
    }

    private void onSelectFromGalleryResult(Intent data) {
        Uri selectedImageUri = data.getData();
        String[] projection = {MediaStore.MediaColumns.DATA};
        Cursor cursor = managedQuery(selectedImageUri, projection, null, null,
                null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();
        String selectedImagePath = cursor.getString(column_index);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(selectedImagePath, options);
        final int REQUIRED_SIZE = 200;
        int scale = 1;
        while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                && options.outHeight / scale / 2 >= REQUIRED_SIZE)
            scale *= 2;
        options.inSampleSize = scale;
        options.inJustDecodeBounds = false;
        thumbnail = BitmapFactory.decodeFile(selectedImagePath, options);
        ShareDialog(thumbnail, editText.getText().toString());
    }

    public void ShareDialog(Bitmap imagePath, String text) {

        if (ShareDialog.canShow(ShareLinkContent.class)) {


            SharePhoto photo = new SharePhoto.Builder()
                    .setBitmap(imagePath)
                    .setCaption(text)
                    .build();
            SharePhotoContent content = new SharePhotoContent.Builder()
                    .addPhoto(photo)
                    .build();

            ShareApi.share(content, null);
        }
        editText.setText("");
    }

    public void DIalog() {
        if (ShareDialog.canShow(ShareLinkContent.class)) {
            ShareLinkContent linkContent = new ShareLinkContent.Builder()

                    .setContentTitle("How to integrate Linkedin from your app")
                    .setImageUrl(Uri.parse(url))
                    .setContentDescription(
                            "simple LinkedIn integration")
                    .setContentUrl(Uri.parse("https://www.numetriclabz.com/android-linkedin-integration-login-tutorial/"))
                    .build();

            shareDialog.show(linkContent);
            set = true;
        }
    }

    private void onCaptureImageResult(Intent data) {
        thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        ShareDialog(thumbnail, editText.getText().toString());
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mPrefs1 = getSharedPreferences("my1",MODE_PRIVATE);
        SharedPreferences.Editor editor = mPrefs1.edit();
        editor.putBoolean("logout", false);
        editor.commit();

    }
}






