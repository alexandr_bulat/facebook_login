package com.example.abulat.userfacebook;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.service.textservice.SpellCheckerService;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphRequestAsyncTask;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.internal.BoltsMeasurementEventListener;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.Arrays;
import java.util.List;

import Utils.PrefUtil;


public class Main extends FragmentActivity {
    CallbackManager callbackManager;
    LoginButton loginButton;
    TextView textView;
    AccessTokenTracker accessTokenTracker;
    AccessToken accessToken;
    ProfileTracker profileTracker;
    PrefUtil prefUtil;
    JSONArray rawName;
    boolean isAccess;

    private SharedPreferences mPrefs;
    private SharedPreferences mPrefs1;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_main);
        loginButton = (LoginButton) findViewById(R.id.login_button);
        List<String> permissionNeeds = Arrays.asList("publish_actions");
        loginButton.setPublishPermissions(permissionNeeds);
        callbackManager = CallbackManager.Factory.create();
        textView = (TextView)findViewById(R.id.textFacebook);
        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {

            }
        };
        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {
                displayWelcomeMassage(currentProfile);
            }
        };

        accessTokenTracker.startTracking();
        profileTracker.startTracking();
        mPrefs = getSharedPreferences("my",MODE_PRIVATE);
        getLoginDetails(loginButton,callbackManager);
        mPrefs1 = getSharedPreferences("my1",MODE_PRIVATE);
        isAccess=mPrefs.getBoolean("access",true);
        if(mPrefs1.getBoolean("logout",true)==false){
            isAccess=true;
        }
        if(isAccess==false) {
            loginButton.setPublishPermissions(permissionNeeds);
            getLoginDetails(loginButton,callbackManager);
            Intent intent=new Intent(this,Friends.class);
            startActivity(intent);


            Toast.makeText(getApplicationContext(),"2",Toast.LENGTH_SHORT);

        }

        else {

            Toast.makeText(getApplicationContext(),"1",Toast.LENGTH_SHORT);
            loginButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    LoginManager.getInstance().logOut();
                    isAccess=false;

                }
            });


            Toast.makeText(getApplicationContext(),"1",Toast.LENGTH_SHORT);
        }


    }
    protected void getLoginDetails(final LoginButton login_button, CallbackManager callbackManager) {

        login_button.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {

            @Override
            public void onSuccess(LoginResult loginResult) {


                SharedPreferences.Editor editor = mPrefs.edit();
                editor.putBoolean("access", false);
                editor.commit();


                    accessToken = loginResult.getAccessToken();
                    Profile profile = Profile.getCurrentProfile();
                    displayWelcomeMassage(profile);
                    GraphRequestAsyncTask graphRequestAsyncTask = new GraphRequest(
                            loginResult.getAccessToken(),
                            "/me/taggable_friends",
                            null,
                            HttpMethod.GET,
                            new GraphRequest.Callback() {
                                public void onCompleted(GraphResponse response) {
                                    isAccess=mPrefs.getBoolean("access",true);
                                    if(isAccess==false) {

                                        try {
                                            rawName = response.getJSONObject().getJSONArray("data");
                                            Intent intent = new Intent(Main.this, Friends.class);
                                            SharedPreferences.Editor editor = mPrefs.edit();
                                            editor.putString("jsondata", rawName.toString());
                                            editor.commit();
                                            startActivity(intent);
                                            if(mPrefs1.getBoolean("logout",true)==false){
                                                isAccess=true;
                                            }

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }

                                }

                            }

                    ).executeAsync();

            }
            @Override
            public void onCancel() {
                Toast.makeText(Main.this,"Cancel",Toast.LENGTH_SHORT).show();



            }
            @Override
            public void onError(FacebookException error) {

            }
        });
    }
    public void displayWelcomeMassage(Profile profile) {

        if (profile != null) {
            textView.setText("Welcome" + profile.getName());
        }

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
    @Override
    public void onResume() {
        super.onResume();
        Profile profile = Profile.getCurrentProfile();
        displayWelcomeMassage(profile);
    }

    @Override
    public void onStop() {
        super.onStop();
        accessTokenTracker.stopTracking();
        profileTracker.stopTracking();
    }



}
