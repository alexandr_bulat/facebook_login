package com.example.abulat.userfacebook;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.share.ShareApi;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by abulat on 7/19/16.
 */
public class DialogView extends Dialog {
    Context context1;
    Bitmap mIcon_val;
    URL newurl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        show();
    }

    public DialogView(Context context) {
        super(context);
        context1 = context;
    }

    public void show(String url) {


        final Dialog dialog = new Dialog(context1);
        dialog.setContentView(R.layout.custom);


        // set the custom dialog components - text, image and button
        TextView text = (TextView) dialog.findViewById(R.id.dismiss);
        TextView textload = (TextView) dialog.findViewById(R.id.loadFoto);
        textload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

            }
        });

        text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });


        // Picasso.with(context1).load(url).into(image1);
        sharePhotoToFacebook(url);


        dialog.show();


    }

    private void sharePhotoToFacebook(String url) {
        Bitmap image = BitmapFactory.decodeResource(context1.getResources(), Integer.parseInt(url));
        SharePhoto photo = new SharePhoto.Builder()
                .setBitmap(image)
                .setCaption("Give me my codez or I will ... you know, do that thing you don't like!")
                .build();

        SharePhotoContent content = new SharePhotoContent.Builder()
                .addPhoto(photo)
                .build();

        ShareApi.share(content, null);

    }


}


