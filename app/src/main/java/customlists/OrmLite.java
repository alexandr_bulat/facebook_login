package customlists;

import com.example.abulat.userfacebook.Mydata;
import com.j256.ormlite.field.DatabaseField;

/**
 * Created by abulat on 7/19/16.
 */
public class OrmLite implements Mydata {

    public static final String FIELD_NAME_ID = "id";

    @DatabaseField(columnName = FIELD_NAME_ID, generatedId = true)
    private int mId;
    @DatabaseField
    private String title;
    @DatabaseField
    private String image;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }


    public OrmLite() {
    }

    public OrmLite(String title, String image) {
        this.title = title;
        this.image = image;

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }


}
