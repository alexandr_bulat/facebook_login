package customlists;

import com.example.abulat.userfacebook.Mydata;

/**
 * Created by abulat on 7/18/16.
 */
public class Friedlist implements Mydata {

    private String title;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    private String image;

    public Friedlist() {
    }

    public Friedlist(String title) {
        this.title = title;

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }
}

