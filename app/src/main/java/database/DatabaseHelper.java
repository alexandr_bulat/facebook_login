package database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import customlists.OrmLite;
import com.example.abulat.userfacebook.R;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import java.sql.SQLException;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "ormlite2.db";
    private static final int DATABASE_VERSION = 2;
    private Dao<OrmLite, Integer> mUserDao = null;
    private RuntimeExceptionDao<OrmLite, Integer> cont;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION, R.raw.armite_config);
    }

    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, OrmLite.class);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource,
                          int oldVersion, int newVersion) {
        try {
            TableUtils.dropTable(connectionSource, OrmLite.class, true);
            onCreate(db, connectionSource);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public Dao<OrmLite, Integer> getUserDao() throws SQLException {
        if (mUserDao == null) {
            mUserDao = getDao(OrmLite.class);
        }

        return mUserDao;
    }

    public RuntimeExceptionDao<OrmLite, Integer> getContcRuntimeExcepctionDao() {
        if (cont == null) {
            cont = getRuntimeExceptionDao(OrmLite.class);
        }
        return cont;
    }

    @Override
    public void close() {
        mUserDao = null;
        super.close();
    }
}