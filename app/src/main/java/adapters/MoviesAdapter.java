package adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.abulat.userfacebook.Mydata;
import com.example.abulat.userfacebook.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.List;

/**
 * Created by abulat on 7/18/16.
 */
public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.MyViewHolder> {

    private List<? extends Mydata> moviesList;
    DisplayImageOptions options;
    Context mcontext;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        private ImageView imageUser;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.info_text);
            imageUser = (ImageView) view.findViewById(R.id.imageView);
        }
    }

    public MoviesAdapter(Context context, List<? extends Mydata> moviesList) {
        this.moviesList = moviesList;
        mcontext = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.usesrs, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Mydata movie = moviesList.get(position);
        holder.title.setText(movie.getTitle());
        options = new DisplayImageOptions.Builder().cacheInMemory().cacheOnDisc().bitmapConfig(
                Bitmap.Config.RGB_565).build();

        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(mcontext));
        imageLoader.displayImage(movie.getImage(), holder.imageUser, options);
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}

